import Consts from '../consts'
import { Home, About, Login } from '../screens'
import Drawer from './stacks/drawer'

const Routes = [
  {
    name: 'Drawer Navigation',
    component: Drawer,
    options: { headerTransparent: true },
  },
  // -> Home
  {
    name: Consts.SCREENS.HOME,
    component: Home,
    options: { headerTransparent: true },
  },
  // -> About
  {
    name: Consts.SCREENS.ABOUT,
    component: About,
    options: { headerTransparent: true },
  },
]

export default Routes
