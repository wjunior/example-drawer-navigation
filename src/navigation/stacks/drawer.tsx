import { createDrawerNavigator } from '@react-navigation/drawer'
import { DrawerStackParamList } from 'navigation/types'
import { Home, About } from '../../screens'
// import { Login } from 'screens'

const Drawer = createDrawerNavigator<DrawerStackParamList>()

function MyDrawer() {
  return (
    <Drawer.Navigator>
      <Drawer.Screen name='Home Page' component={Home} />
      <Drawer.Screen name='About Page' component={About} />
      {/* <Drawer.Screen name='Login Page' component={Login} /> */}
    </Drawer.Navigator>
  )
}

export default MyDrawer
