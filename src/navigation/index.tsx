import * as React from 'react'
import { Platform } from 'react-native'
import { NavigationContainer } from '@react-navigation/native'
import { createNativeStackNavigator } from '@react-navigation/native-stack'

import { RootStackParamList } from './types'
import { navigationRef } from './RootNavigation'
import Constants from '../consts'

import routes from './routes'
import Drawer from './stacks/drawer'
import { Login } from '../screens'

const RootStack = createNativeStackNavigator<RootStackParamList>()

interface RouteProps {
  name: string
  component: React.ComponentType<any>
  options?: object
}

function Routes() {
  const paddingLeft = Platform.select({ ios: 16, android: 0 })

  return (
    <NavigationContainer ref={navigationRef}>
      <RootStack.Navigator screenOptions={{ headerShown: false }}>
        <RootStack.Screen
          key={Constants.SCREENS.LOGIN}
          name='Login'
          component={Login}
        />
        <RootStack.Screen
          key={Constants.SCREENS.HOME}
          name='Home'
          component={Drawer}
        />
        {/* {routes.map(({ name, component, options }: RouteProps) => {
          return (
            <RootStack.Screen
              key={name}
              name={name as keyof RootStackParamList}
              component={component}
              options={options}
            />
          )
        })} */}
      </RootStack.Navigator>
    </NavigationContainer>
  )
}

export default Routes
