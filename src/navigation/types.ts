import { RouteProp } from '@react-navigation/native'
import { DrawerNavigationProp } from '@react-navigation/drawer'
import { NativeStackNavigationProp } from '@react-navigation/native-stack'

export type RootStackParamList = {
  Home: undefined
  About: undefined
  Login: undefined
}

export type DrawerStackParamList = {
  'Home Page': undefined
  'About Page': undefined
}

// export type HomeScreenRouteProp = RouteProp<RootStackParamList, 'Home'>
export type RootRoutesProp = NativeStackNavigationProp<
  RootStackParamList,
  'Home'
>
export type DrawerRouteProp = DrawerNavigationProp<
  DrawerStackParamList,
  'Home Page',
  'About Page'
>
