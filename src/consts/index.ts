const constants = {
  // -> SCREENS
  SCREENS: {
    HOME: 'Home',
    ABOUT: 'About',
    LOGIN: 'Login',
  },
} as const

const Consts: typeof constants = constants

export default Consts
