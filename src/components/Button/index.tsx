import * as S from './styles'

interface ButtonProps {
  label: string
  onPress: () => void
  children?: React.ReactNode
}

const Button = ({ label, onPress }: ButtonProps) => {
  return (
    <S.Button onPress={onPress}>
      <S.Text>{label}</S.Text>
    </S.Button>
  )
}
export default Button
