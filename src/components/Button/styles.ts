import styled from 'styled-components/native'

export const Button = styled.TouchableOpacity`
  width: 50%;
  border-radius: 16px;
  text-align: center;
  border: solid 1px #22bed9;
`

export const Text = styled.Text`
  font-size: 20px;
  text-align: center;
  text-transform: uppercase;
`
