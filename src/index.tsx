import * as React from 'react'

import Main from './navigation'

function App() {
  return <Main />
}

export default App
