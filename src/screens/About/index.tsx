import * as S from './styles'

const About = () => (
  <S.Container>
    <S.Text>About</S.Text>
  </S.Container>
)

export default About
