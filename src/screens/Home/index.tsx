import { useRef, useMemo, useCallback } from 'react'
import * as S from './styles'
import BottomSheet from '@gorhom/bottom-sheet'

const Home = () => {
  const bottomSheetRef = useRef<BottomSheet>(null)
  const snapPoints = useMemo(() => ['25%', '50%'], [])
  const handleSheetChanges = useCallback((index: number) => {
    console.log('handleSheetChanges', index)
  }, [])

  return (
    <S.Container>
      <S.Text>HOME</S.Text>
      <BottomSheet
        ref={bottomSheetRef}
        index={1}
        snapPoints={snapPoints}
        onChange={handleSheetChanges}>
        <S.ModalContent>
          <S.Text>Awesome 🎉</S.Text>
        </S.ModalContent>
      </BottomSheet>
    </S.Container>
  )
}

export default Home
