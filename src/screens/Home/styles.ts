import styled from 'styled-components/native'

export const Container = styled.View`
  flex: 1;
  padding-top: 50px;
`

export const Text = styled.Text`
  font-size: 20px;
  text-align: center;
`

export const ModalContent = styled.View`
  flex: 1;
  align-items: center;
`
