import * as S from './styles'
import { useNavigation } from '@react-navigation/native'
// import { Button } from 'components'
import { Button } from '../../components'
import { RootRoutesProp } from 'navigation/types'

const Login = () => {
  const navigation = useNavigation<RootRoutesProp>()
  return (
    <S.Container>
      <S.Text>Login</S.Text>
      <Button label='Entrar' onPress={() => navigation.navigate('Home')} />
    </S.Container>
  )
}

export default Login
