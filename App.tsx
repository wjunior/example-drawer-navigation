import 'react-native-gesture-handler'
import { StatusBar } from 'expo-status-bar'

import App from './src'

export default () => {
  return (
    <>
      <StatusBar style='dark' />
      <App />
    </>
  )
}
